import random
import time
import os


def clear():
    """Clears the terminal"""
    os.system("cls;clear")


word = list(input("Enter word: "))

alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM`12345678900-=~!@#$%^&*()_+[]{};'\\:\"|,./<>?£"

currentWord = [" " for i in word]
iterations = 0

while True:
    clear()
    for i, letter in enumerate(word):
        if currentWord[i] != letter:
            currentWord[i] = random.choice(alphabet)
    for character in currentWord:
        print(character, end="")
    print()
    if currentWord == word:
        break
    iterations += 1
    time.sleep(0.01)

print(f"The program took {iterations} iterations")
